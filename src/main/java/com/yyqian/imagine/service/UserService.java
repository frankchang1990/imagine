package com.yyqian.imagine.service;

import com.yyqian.imagine.dto.PasswordRestForm;
import com.yyqian.imagine.dto.UserCreateForm;
import com.yyqian.imagine.dto.UserUpdateForm;
import com.yyqian.imagine.po.AppUser;

import java.util.Collection;
import java.util.Optional;

/**
 * Created on 12/15/15.
 *
 * @author Yinyin Qian
 */
public interface UserService {

  Optional<AppUser> getUserById(long id);

  Optional<AppUser> getUserByUsername(String username);

  Collection<AppUser> getAllUsers();

  AppUser create(UserCreateForm form);

  int increasePointByUserId(Long id);

  AppUser update(UserUpdateForm form);

  AppUser resetPassword(PasswordRestForm form);

  void sendRestPasswordEmail(String username);

  void authUserByToken(String token);

}
