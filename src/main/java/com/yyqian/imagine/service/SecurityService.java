package com.yyqian.imagine.service;

import com.yyqian.imagine.po.AppUser;

/**
 * Created on 12/15/15.
 *
 * @author Yinyin Qian
 */
public interface SecurityService {

  String getUsername();

  String getAuthorities();

  AppUser getUser();

  boolean isLoggedIn();

}
